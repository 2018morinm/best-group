import librosa

def change_tempo(f_entre, rate):
    """
    Fonction pour accelerer/ralentir une chanson avec phase vocoder
    On change pas le sample rate

    :param f_entre: valeur lu d'après un fichier audio (mp3, wav, etc ...)
    :param rate: bpm(entree)/bpm(sortie)
    :return: chanson modifie (même type que f_entre)
    """
    D = librosa.stft(f_entre, n_fft=4096, hop_length=512) # STFT
    D_fast = librosa.phase_vocoder(D, rate, hop_length=512) # Vocoder
    chanson_modifiee = librosa.istft(D_fast, hop_length=512) # Retour à domaine temps
    return chanson_modifiee

def detect_rate(f_entre_1, f_entre_2, sr1, sr2):
    """
    Fonction pour calculer la raison en bpm de deux chansons

    :param f_entre_1: Fichier de reference
    :param f_entre_2: Fichier pour calculer le retard/avance
    :param sr1: Sample rate fichier reference
    :param sr2: Sample rate fichier modifiable
    :return: raison entre bpm
    """
    tempo_chanson_1 = librosa.beat.tempo(y=f_entre_1, sr=sr1)
    tempo_chanson_2 = librosa.beat.tempo(y=f_entre_2, sr=sr2)
    return tempo_chanson_1/tempo_chanson_2

def ajuster_tempo(f_entre_1, f_entre_2, sr1, sr2):
    """
    Fonction pour ajuster le bpm entre deux chansons

    Mêmes paramétres d'entree que detect_rate
    :return: Chanson modifiee
    """

    rate = detect_rate(f_entre_1, f_entre_2, sr1, sr2)
    return change_tempo(f_entre_2, rate)

