from tkinter import *
from tkinter.filedialog import askopenfilename
import librosa
import changement_tempo_pitch as ctp
import changement_chorus as dc
import os

chanson = {"verse": "", "refrain": ""}
pitch_shift_refrain = 0
pitch_shift_verse = 0

# Fenetre Principal
fenetre = Tk()
fenetre.title("Mixer")
fenetre.config(bg="#6699ff")
fenetre.geometry("500x600")

# Lire frames gif
frames_gif = [PhotoImage(file='DjChat.gif',format = 'gif -index %i' %(i)) for i in range(42)]

# Commands pour les buttons. Pas de valeur retourne parce que tkinter utilise les fonctions comme procedures
def select_song_chorus(name):
    """
    Procedure pour les buttons de selection de fichier de refrain

    :param name: Nom du fichier choisi
    :return: Pas de valeur retourné, c'est juste un procedure pour le button.
    """
    FILETYPES = [("wav files", "*.wav"),("mp3 files","*.mp3")]
    filename = StringVar(fenetre)
    filename.set(askopenfilename(filetypes=FILETYPES))
    chanson[name] = filename.get()
    button_fichier1 = Button(frame_button, text="..."+chanson[name][-15:], command=lambda *args: select_song_chorus("refrain"))
    button_fichier1.grid(row=0, column=0, padx=5, pady=5)
    print(chanson)

def select_song_verse(name):
    """
    Procedure pour les buttons de selection de fichier de refrain

    :param name: Nom du fichier choisi
    :return: Pas de valeur retourné, c'est juste un procedure pour le button.
    """
    FILETYPES = [("wav files", "*.wav"),("mp3 files","*.mp3")]
    filename = StringVar(fenetre)
    filename.set(askopenfilename(filetypes=FILETYPES))
    chanson[name] = filename.get()
    button_fichier2= Button(frame_button, text="..."+chanson[name][-15:], command=lambda *args: select_song_verse("verse"))
    button_fichier2.grid(row=1, column=0, padx=5, pady=5)
    print(chanson)


def traitement_tempo():
    """
    Procedure pour aligner les bpms de deux chanson. Toujours change le bpm de la chanson de refrain et utilise les
    fonctions en changement tempo_pitch

    :return:
    """
    if (chanson["verse"] != ""): # Lecture fichier chanson verse
        fichier_verse, sr_verse = librosa.core.load(chanson["verse"],
                                                    sr=None,
                                                    duration=librosa.core.get_duration(filename=chanson["verse"]))

    if (chanson["refrain"] != ""): # Lecture fichier chanson refrain
        fichier_refrain, sr_refrain = librosa.core.load(chanson["refrain"],
                                                        sr=None,
                                                        duration=librosa.core.get_duration(filename=chanson["refrain"]))

    if (chanson["verse"] != "") and (chanson["refrain"] != ""): # Ajuste tempo
        fichier_traite =  ctp.ajuster_tempo(fichier_verse, fichier_refrain, sr_verse,  sr_refrain) # Appel a fonction pour ajuster tempo
        librosa.output.write_wav("sortie_tempo.wav", fichier_traite, sr_refrain)
    os.system("open -a \"QuickTime Player\" sortie_tempo.wav") # Command pour démarrer la chanson
    # Important: On utilise Quicktime parce que on a eu des problèmes avec la bibliothèque iTunes.
    # Pour un produit final, il faudra utiliser le player default d'utilisateur

def traitement_pitch_refrain():
    """
    Fonction pour faire des changements de pitch dans une chanson.

    :return:
    """
    global pitch_shift_refrain
    pitch_shift_refrain = slider_refrain.get() # Prendre le valeur du slider avec le pitch shift
    fichier_refrain, sr_refrain = librosa.core.load(chanson["refrain"], sr=None)  # Lecture chanson
    fichier_modifie = librosa.effects.pitch_shift(fichier_refrain, sr_refrain, pitch_shift_refrain, 12)
    librosa.output.write_wav("sortie_pitch_refrain.wav", fichier_modifie, sr_refrain)
    os.system("open -a \"QuickTime Player\" sortie_pitch_refrain.wav")

def traitement_pitch_verse():
    """
    Identique à traitement pitch refrain, sauf que dans cette fonction on travaille sur le verse

    :return:
    """
    global pitch_shift_verse
    pitch_shift_verse = slider_verse.get()
    fichier_verse, sr_verse = librosa.core.load(chanson["verse"], sr=None)
    fichier_modifie = librosa.effects.pitch_shift(fichier_verse, sr_verse, pitch_shift_verse, 12)
    librosa.output.write_wav("sortie_pitch_verse.wav", fichier_modifie, sr_verse)
    os.system("open -a \"QuickTime Player\" sortie_pitch_verse.wav")

def mixage():
    """
    Procedure pour le button de mixage. Il appelle les fonctions de mixage sur changement_chorus

    :return:
    """
    if (chanson["verse"] != "") and (chanson["refrain"] != ""):
        dc.chanson_sans_refrain(chanson["verse"])
        dc.isole_chorus(chanson["refrain"],20)
        dc.mix("chanson_sans_refrain.wav","refrain.wav")
        os.system("rm chanson_sans_refrain.wav refrain.wav")
        os.system("open -a \"QuickTime Player\" mix.wav")

def update_gif(index):
    """
    Procedure pour animer le gif de titre de la page.

    :param index: Frame de l'animation gif. Int
    :return:
    """
    frame_gif = frames_gif[index] # On prend le frame actuel
    if index < 41:
        index += 1 # Increment contage de frames
    else:
        index = 0
    title_image.configure(image=frame_gif) # Changement de l'image montrée
    fenetre.after(100, update_gif, index) # Appel au procedure pour continuer

# Titre Page
Label(fenetre, text="Le Mixer", bg="#6699ff", font=("AppleGothic", 50)).pack(side=TOP)

# Title Image
title_image = Label(fenetre, bg="#6699ff",height=275)
title_image.pack(side=TOP)
fenetre.after(0, update_gif, 0)

# Buttons frame
frame_button = Frame(fenetre, bg="#6699ff", bd=0, width=600, height=600, pady=10)
frame_button.pack(side=TOP)

# Buttons et Sliders Refrain
button_fichier1 = Button(frame_button, text="Fichier Refrain", command=lambda *args: select_song_chorus("refrain"))
button_fichier1.grid(row=0, column=0, padx=5, pady=5)
slider_refrain = Scale(frame_button, from_=-12, to=12, orient=HORIZONTAL, label= "Slider Refrain")
slider_refrain.grid(row=0, column=1, padx =5, pady=5)
button_pitch_refrain = Button(frame_button, text="Ajuste Pitch Refrain", command=traitement_pitch_refrain)
button_pitch_refrain.grid(row=0, column=2, padx = 5, pady=5)

# Buttons et Sliders Verse
button_fichier2= Button(frame_button, text ="Fichier Verse", command=lambda *args: select_song_verse("verse"))
button_fichier2.grid(row=1, column=0, padx=5, pady=5)
slider_verse = Scale(frame_button, from_=-12, to=12, orient=HORIZONTAL, label= "Slider Verse")
slider_verse.grid(row=1, column=1, padx=5, pady=5)
button_pitch_verse = Button(frame_button, text="Ajuste Pitch Verse", command=traitement_pitch_verse)
button_pitch_verse.grid(row=1, column=2, padx=5, pady=5)

# Autres Buttons
button_tempo = Button(frame_button, text="Ajuste Tempo", command=traitement_tempo)
button_tempo.grid(row=2, column=0, padx=5, pady=5)
button_mixage = Button(frame_button, text="Mixage", command=mixage)
button_mixage.grid(row=2, column=2, padx=5, pady=5)

# Loop Fenetre
fenetre.mainloop()

