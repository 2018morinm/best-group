# Le Mixer - best group
Notre projet est un mixer de chansons avec interface graphique en Python. Il enlève le refrain d’une chanson et le met à la place du refrain d’une deuxième chanson. Aussi, il permet de faire des modifications de tempo, pour donner aux chansons le même bpm, et de pitch, pour ajuster la tonalité de chaque chanson

## Getting started
Télécharger le repo et démarrer le fichier interface.py avec python

## Prerequisites
* librosa
* pydub
* pychorus
* tkinter

Ces bibliothèques python peuvent être instalées avec pip. Marche avec Python 3.6

## Authors
Hatim Benchaaboun
Elio Garnaoui
Magali Morin
Hugo Prudent
Bernardo Vieira de Miranda
