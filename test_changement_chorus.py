import pytest
import changement_chorus as dc
import os

def test_temps_refrain():
    assert isinstance(dc.temps_refrain("wit.mp3"), float)
    assert isinstance(dc.temps_refrain("thunder.mp3"), float)
    assert os.path.isfile("refrain.wav") == True

def test_chanson_sans_refrain():
    dc.chanson_sans_refrain("thunder.mp3")
    assert os.path.isfile("chanson_sans_refrain.wav") == True

def test_mix():
    dc.chanson_sans_refrain("thunder.mp3")
    dc.isole_chorus("wit.mp3",20)
    dc.mix("chanson_sans_refrain.wav","refrain.wav")
    assert os.path.isfile("mix.wav") == True


