import pytest
import numpy as np
import librosa
import changement_tempo_pitch as ctp

def test_change_tempo():
    y, sr = librosa.core.load("thunder.mp3", sr=None)
    y_slower = ctp.change_tempo(y, 0.5)
    y_faster = ctp.change_tempo(y,2)
    assert isinstance(y_slower, np.ndarray)
    assert isinstance(y_faster, np.ndarray)
    assert librosa.core.get_duration(y=y_slower, sr=sr) > librosa.core.get_duration(y=y, sr=sr)
    assert librosa.core.get_duration(y=y_faster, sr=sr) < librosa.core.get_duration(y=y, sr=sr)

def test_detect_rate():
    y1, sr1 = librosa.core.load("thunder.mp3", sr=None)
    y2, sr2 = librosa.core.load("wit.mp3", sr=None)
    assert ctp.detect_rate(y1,y2,sr1,sr2) < 1

def ajuste_tempo():
    y1, sr1 = librosa.core.load("thunder.mp3", sr=None)
    y2, sr2 = librosa.core.load("wit.mp3", sr=None)
    y2_ajuste = ctp.ajuster_tempo(y1,y2,sr1,sr2)
    assert librosa.core.get_duration(y=y2_ajuste, sr=sr2) > librosa.core.get_duration(y=y2, sr=sr2)


