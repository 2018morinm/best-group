from pychorus import find_and_output_chorus
from pydub import AudioSegment
import librosa
from pychorus import find_and_output_chorus

def temps_refrain(file):
    """
    Fonction pour detecter le debut du refrain

    :param file: Fichier de chanson qu'on souhaite trouver le refrain. String avec endroit du fichier
    :return: Temps du début du refrain
    """
    chorus_start_sec = find_and_output_chorus(file,"refrain.wav",20)
    return(chorus_start_sec)


def chanson_sans_refrain(file):
    """
    Fonction pour enlever le refrain. Elle va creer un fichier avec la chanson choisi sans son refrain

    :param file: Fichier de chanson qu'on souhaite enlever le refrain. String avec endroit du fichier
    :return: Ne retourne rien. Par contre cette fonction écrit un fichier .wav avec la chanson sans refrain
    """
    y, sr = librosa.load(file, duration = temps_refrain(file)+1)
    librosa.output.write_wav("chanson_sans_refrain.wav",y,sr)
    return None


def isole_chorus(chanson,duree) :
    """
    Fonction pour isoler le refrain. Elle va creer un fichier avec le refrain de la chanson choisi

    :param chanson: Fichier de chanson qu'on souhaite isoler le refrain. String avec endroit du fichier
    :param duree: Duree estime du refrain, secondes
    :return: None. Par contre écrit un fichier .wav avec le refrain
    """
    chorus_start_sec = find_and_output_chorus(chanson, "refrain.wav",duree)
    return None


def mix(chanson_verse, chanson_refrain):
    """
    Fonction pour faire le mix entre les deux fichier audio, verse et refrain. Utilise crossfade.

    :param chanson_verse: Fichier de la chanson avec le verse seulement. String avec le endroit du fichier
    :param chanson_refrain: Fichier de la chanson avec le refrain seulement. String avec le endoir du fichier
    :return: None
    """
    sound1 = AudioSegment.from_file(chanson_verse)
    sound2 = AudioSegment.from_file(chanson_refrain)
    crossfade = sound1.append(sound2, crossfade=1000)
    crossfade.export("mix.wav", format="wav")
    return None
